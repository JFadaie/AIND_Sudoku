# Artificial Intelligence Nanodegree
## Introductory Project: Diagonal Sudoku Solver

# Question 1 (Naked Twins)
Q: How do we use constraint propagation to solve the naked twins problem?  
A: In general, constrain propogation is about leveraging relationships between
entities in a given problem domain to minimize effort (time, memory, etc. )
needed to find a solution. In this case, constraint propogation regards
leveraging the relationships between various boxes on the Sudoku board given
the rules of the Sudoku game. 

The naked twin problem helps constrain the search 
space of the Sudoku board (the search space being the tree of 
possible Sudoku board states), by removing possible values that 
a Sudoku box can display. Specifically, if a unit contains two boxes, both with
two identical possible values, then all other boxes within the unit cannot 
contain these values. Only the "twin" boxes can contain one of these two values. 
Thus,the Naked Twin problem constrains the possible values Sudoku boxes within a unit 
can take, minimizing the number of permutations of the board.

# Question 2 (Diagonal Sudoku)
Q: How do we use constraint propagation to solve the diagonal sudoku problem?  
A: In general, constrain propogation is about leveraging relationships between
entities in a given problem domain to minimize effort (time, memory, etc. )
needed to find a solution. In this case, constraint propogation regards
leveraging the relationships between various boxes on the Sudoku board given
the rules of the Sudoku game.

The Diagonal Sudoku problem adds another relationship/rule on top of the regular 
Sudoku puzzle. Initially, solving Sudoku requires that all of the units: 
being columns, rows, and squares (each containing 9 boxes), must display a 
single value unique to that unit. As an example, if a box already displays the 
value 9, no other boxes within that boxes units (peers) can contain the value 9.
To solve Diagonal Sudoku, another constraint was added such that the top left 
to bottom right, and bottom left to top right diagonal units need to contain 
unique values in their boxes. The two diagonal units were thus added to the list 
of original units: rows, columns, and squares, amending the search space. Logical
constraints such as Naked Twins, Eliminate one, and Only Choice were then 
applied repeatedly to this updated list of units within the board, minimizing
the potential values each box could take until only a single value remained for each.



### Install

This project requires **Python 3**.

We recommend students install [Anaconda](https://www.continuum.io/downloads), a pre-packaged Python distribution that contains all of the necessary libraries and software for this project. 
Please try using the environment we provided in the Anaconda lesson of the Nanodegree.

##### Optional: Pygame

Optionally, you can also install pygame if you want to see your visualization. If you've followed our instructions for setting up our conda environment, you should be all set.

If not, please see how to download pygame [here](http://www.pygame.org/download.shtml).

### Code

* `solutions.py` - You'll fill this in as part of your solution.
* `solution_test.py` - Do not modify this. You can test your solution by running `python solution_test.py`.
* `PySudoku.py` - Do not modify this. This is code for visualizing your solution.
* `visualize.py` - Do not modify this. This is code for visualizing your solution.

### Visualizing

To visualize your solution, please only assign values to the values_dict using the ```assign_values``` function provided in solution.py

### Data

The data consists of a text file of diagonal sudokus for you to solve.